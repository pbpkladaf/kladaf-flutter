import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class DiaryItem extends StatelessWidget {
  final String id;
  final String title;
  final String dateCreated;
  final String content;
  final String user;

  DiaryItem(
    this.id,
    this.title,
    this.dateCreated,
    this.content,
    this.user,
  );

  void deleteDiaryItem(String id) async {
    String pk = id;
    String url =
        'https://kladaf-app.herokuapp.com/diary/api/delete-flutter/$pk';
    try {
      final response = await http.get(Uri.parse(url));
      print(response.statusCode);
    } catch (error) {
      print(error);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          ListTile(
            title: Text(dateCreated),
          ),
          ButtonBar(
            children: <Widget>[
              ElevatedButton(
                child: const Text(
                  'View',
                ),
                onPressed: () {
                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return Dialog(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20.0)),
                          child: Container(
                            constraints: const BoxConstraints(maxHeight: 350),
                            child: Padding(
                              padding: const EdgeInsets.all(12.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  RichText(
                                    textAlign: TextAlign.justify,
                                    text: TextSpan(
                                        text: content,
                                        style: const TextStyle(
                                            fontWeight: FontWeight.w400,
                                            fontSize: 16,
                                            color: Colors.black,
                                            wordSpacing: 1)),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                      });
                },
              ),
              ElevatedButton(
                  style: ElevatedButton.styleFrom(primary: Colors.red),
                  child: const Text(
                    'Delete',
                  ),
                  onPressed: () {
                    deleteDiaryItem(id);
                  })
            ],
          ),
        ],
      ),
    );
  }
}
