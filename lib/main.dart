import 'package:flutter/material.dart';
import 'package:kladafmobile/session.dart';
// import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
// import 'package:kladafmobile/network_request.dart';
import 'package:kladafmobile/screens/login.dart';
import 'package:kladafmobile/screens/users.dart';
import 'package:shared_preferences/shared_preferences.dart';
// import 'package:provider/provider.dart';
import '../screens/home.dart';
import 'screens/register.dart';
import 'models/diary.dart';

class Env {
  static bool isLoggedIn = false;
  static Map<dynamic, dynamic> userData = {
    "name": "",
    "hobby": "",
  };
}

Future<void> main() async {
  await is_authenticated()
      .then((result) => Env.isLoggedIn = result)
      .catchError((e) => {runApp(MyApp())});
  await getUserData().then((result) {
    Env.userData['name'] = result['name'];
    Env.userData['hobby'] = result['hobby'];
  }).catchError((e) => {runApp(MyApp())});
  print(Env.userData);
  runApp(const MyApp());
  // runApp(RestartWidget(
  //   child: MyApp(),
  // ));
}

// void main() {
//   runApp(const MyApp());
// }

// dummy userId -- Change with fetchUserId() if login module finished
String uid = "0";

// Diary Method & Var
List<dynamic> diaryData = [];
List<Diary> diaryList = [];

void fetchDiaryData() async {
  const url =
      'https://kladaf-app.herokuapp.com/diary/api/diarySet/?format=json';
  try {
    final response = await http.get(Uri.parse(url));
    if (response.statusCode == 200) {
      List<dynamic> extractedData = jsonDecode(response.body);
      diaryData = extractedData;
    }
  } catch (error) {
    print(error);
  }
}

List<Diary> buildDiary() {
  diaryList = [];
  for (var element in diaryData) {
    if (element['user'].toString() == uid) {
      diaryList.add(Diary(
        id: element['id'].toString(),
        title: element['judul'],
        dateCreated: element['tanggal'].toString(),
        content: element['isi'],
        userId: element['user'].toString(),
      ));
    }
  }
  return diaryList;
}

Future<bool> setUserId() async {
  final prefs = await SharedPreferences.getInstance();
  if (prefs.getInt('user_id') != null) {
    uid = prefs.getInt('user_id').toString();
    return true;
  }
  return false;
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // return Provider(
    return MaterialApp(
      // child: MaterialApp(
      title: 'Kladaf',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Env.isLoggedIn == true ? Home() : RegisterScreen(),
      // home: const Home(),
      // ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const <Widget>[
            Text(
              'Kladaf',
            ),
          ],
        ),
      ),
    );
  }
}
