import 'package:flutter/material.dart';
import 'package:kladafmobile/screens/progresstracker_taskscreen.dart';
import '../widgets/progresstracker_statusbutton.dart';
import '../widgets/navbar.dart';
import 'progresstracker_form.dart';
import 'progresstracker_constants.dart';
import 'package:http/http.dart' as http;
import '../models/progresstracker.dart';
import 'dart:convert';

class TaskList extends StatefulWidget {
  const TaskList({Key? key}) : super(key: key);

  @override
  HomeState createState() => HomeState();
}

class HomeState extends State<TaskList> {
  List<Task>? task_list;
  var task_title = TextEditingController();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      getSectionData(context);
    });
  }

  Future<void> getSectionData(BuildContext context) async {
    var response = await http.get(
        Uri.parse(
            "https://kladaf-app.herokuapp.com/progress-tracker/all-tasks-api/"),
        headers: {
          "Accept": "aplication/json",
        });
    print(response.body);
    if (response.statusCode == 200) {
      setState(() {
        print('success');
        task_list = taskFromJson(response.body);
      });
    } else {
      print("Error occured");
    }
    // for (int i = 0; i < 5; i++) {
    //   print(task_list);
    // }
  }

  // Future<void> postSectionData(BuildContext context) async {
  //   var response = await http.post(
  //       Uri.parse(
  //           "https://kladaf-app.herokuapp.com/progress-tracker/add-task-api"),
  //       headers: {
  //         "content-type": "application/json;charset=UTF-8",
  //       },
  //       body: jsonEncode({'task': task_title.text, 'status' : }));
  //   print(response.body);
  //   if (response.statusCode == 200) {
  //     setState(() {
  //       print('success');
  //       sectionsLst = sectionFromMap(response.body);
  //     });
  //   } else {
  //     print("Error occured");
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xff0d1b2a),
        drawer: MainDrawer(),
        body: Container(
            color: Color(0xff0d1b2a),
            child: Padding(
                padding: EdgeInsets.fromLTRB(30, 60, 30, 20),
                child: ListView(children: <Widget>[
                  Container(
                      alignment: Alignment.center,
                      padding: EdgeInsets.all(10),
                      child: Column(children: <Widget>[
                        for (var i = 0; i < 8; i++)
                          Card(
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(15.0))),
                            elevation: 1.9,
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15.0),
                                color: Color(0xFF778da9),
                              ),
                              padding: EdgeInsets.all(7.0),
                              child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                    Text('This is my list ',
                                        style: TextStyle(
                                            color: Color(0xFFe0e1dd),
                                            fontSize: 20,
                                            fontWeight: FontWeight.bold)),
                                  ]),
                              height: 75.0,
                              width: 275.0,
                              // child: ElevatedButton.icon(
                              //     onPressed: () {
                              //       Navigator.push(
                              //         context,
                              //         MaterialPageRoute(
                              //             builder: (context) => const FormPage()),
                              //       );
                              //     },
                              //     icon: Icon(Icons.edit),
                              //     label: Text(
                              //       'Edit',
                              //       style: TextStyle(
                              //         fontFamily: 'Segoe UI',
                              //         fontSize: 12,
                              //         fontWeight: FontWeight.bold,
                              //       ),
                              //     ),
                              //     style: ElevatedButton.styleFrom(
                              //         fixedSize: const Size(12, 12))),
                            ),
                          )
                      ])),
                  Container(
                    child: Container(
                      padding: EdgeInsets.fromLTRB(8, 8, 0, 0),
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          onPrimary: Colors.white,
                          primary: Color(0xFF748ba5),
                        ),
                        child: Text('Back'),
                        onPressed: () {
                          Navigator.pop(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => TaskScreen()));
                        },
                      ),
                    ),
                  )
                ]))));
  }
}
