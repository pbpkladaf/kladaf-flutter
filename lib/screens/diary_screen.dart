import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../widgets/diary_item.dart';
import '../main.dart';

class DiariesScreen extends StatelessWidget {
  DiariesScreen({Key? key}) : super(key: key);

  final _formKey = GlobalKey<FormState>();
  String inputTitle = '';
  String inputContent = '';

  GridView diaryCards = GridView(
    gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
      maxCrossAxisExtent: 200,
      childAspectRatio: 3 / 2,
      crossAxisSpacing: 20,
      mainAxisSpacing: 20,
    ),
  );

  GridView buildDiaryCard() {
    return GridView(
      children: diaryList
          .map(
            (catData) => DiaryItem(
              catData.id,
              catData.title,
              catData.dateCreated,
              catData.content,
              catData.userId,
            ),
          )
          .toList(),
      gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
        maxCrossAxisExtent: 240,
        childAspectRatio: 3 / 2,
        crossAxisSpacing: 20,
        mainAxisSpacing: 20,
      ),
    );
  }

  void addDiary(String judul, String isi, String userId) async {
    try {
      DateTime today = new DateTime.now();
      String dateMaker =
          "${today.year.toString()}-${today.month.toString().padLeft(2, '0')}-${today.day.toString().padLeft(2, '0')}";
      Map data = {
        "judul": judul,
        "tanggal": dateMaker,
        "isi": isi,
        "user": userId,
      };
      String body = jsonEncode(data);
      String url = 'https://kladaf-app.herokuapp.com/diary/api/diarySet/';
      final response = await http.post(Uri.parse(url),
          headers: {'content-type': 'application/json'}, body: body);
    } catch (error) {
      print(error);
    }
  }

  void deleteAllDiary(String userId) async {
    String url = 'https://kladaf-app.herokuapp.com/diary/api/delete-all/$uid';
    try {
      final response = await http.get(Uri.parse(url));
      print(response.statusCode);
    } catch (error) {
      print(error);
    }
  }

  Future<bool> setUserId() async {
    final prefs = await SharedPreferences.getInstance();
    if (prefs.getInt('user_id') != null) {
      uid = prefs.getInt('user_id').toString();
      return true;
    }
    return false;
  }

  @override
  Widget build(BuildContext context) {
    diaryCards = buildDiaryCard();
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xff0d1b2a),
        title: const Text(
          'Kladaf Diary',
          style: TextStyle(
            color: Color(0xFFe0e1dd),
            fontFamily: 'Segoe UI',
          ),
        ),
        actions: <Widget>[
          FlatButton(
            textColor: Colors.white,
            onPressed: () {
              deleteAllDiary(uid);
            },
            child: const Text("Delete All"),
            shape:
                const CircleBorder(side: BorderSide(color: Colors.transparent)),
          ),
          FlatButton(
            onPressed: () {
              fetchDiaryData();
              diaryList = buildDiary();
              diaryCards = buildDiaryCard();
            },
            child: const Icon(
              Icons.refresh_rounded,
              color: Color(0xFFe0e1dd),
            ),
            shape:
                const CircleBorder(side: BorderSide(color: Colors.transparent)),
          ),
        ],
      ),
      backgroundColor: const Color.fromARGB(255, 27, 38, 59),
      body: Padding(padding: const EdgeInsets.all(25), child: diaryCards),
      floatingActionButton: FloatingActionButton.extended(
        backgroundColor: const Color.fromARGB(255, 65, 90, 119),
        foregroundColor: const Color.fromARGB(255, 224, 225, 221),
        onPressed: () {
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  content: Stack(
                    clipBehavior: Clip.none,
                    fit: StackFit.loose,
                    children: <Widget>[
                      Form(
                        key: _formKey,
                        child: Container(
                          padding: const EdgeInsets.all(20.0),
                          child: Column(
                            children: [
                              TextFormField(
                                decoration: InputDecoration(
                                  labelText: 'Title',
                                  icon: const Icon(Icons.people),
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5.0),
                                  ),
                                ),
                                validator: (value) {
                                  if (value!.isEmpty) {
                                    return 'Please Enter The Title';
                                  }
                                  return null;
                                },
                                onSaved: (value) {
                                  inputTitle = value!;
                                },
                              ),
                              TextFormField(
                                decoration: InputDecoration(
                                  labelText: 'Content',
                                  icon: const Icon(Icons.lock),
                                  border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5.0),
                                  ),
                                ),
                                validator: (value) {
                                  if (value!.isEmpty) {
                                    return 'Please Enter The Content';
                                  }
                                  return null;
                                },
                                onSaved: (value) {
                                  inputContent = value!;
                                },
                              ),
                              ButtonBar(
                                children: [
                                  ElevatedButton(
                                    child: const Text(
                                      "Submit",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                    onPressed: () {
                                      if (_formKey.currentState!.validate()) {
                                        _formKey.currentState!.save();
                                      }
                                      if (inputTitle != '' &&
                                          inputContent != '') {
                                        addDiary(inputTitle, inputContent, uid);
                                      }
                                      fetchDiaryData();
                                      diaryList = buildDiary();
                                      diaryCards = buildDiaryCard();
                                      Navigator.of(context).pop();
                                    },
                                  ),
                                  ElevatedButton(
                                    child: const Text(
                                      "Cancel",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                    onPressed: () {
                                      fetchDiaryData();
                                      diaryList = buildDiary();
                                      diaryCards = buildDiaryCard();
                                      Navigator.of(context).pop();
                                    },
                                  ),
                                ],
                              )
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                );
              });
        },
        icon: const Icon(Icons.add),
        label: const Text('Diary'),
      ),
    );
  }
}
