import 'package:flutter/material.dart';
import '../models/rating.dart';
import '../models/menu.dart';
import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import '../widgets/navbar.dart';

class RateJson {
  String model = "";
  int pk = 0;

  Map<String, dynamic> fields = {};

  RateJson(this.model, this.pk, this.fields);

  RateJson.fromJson(Map<String, dynamic> json) {
    model = json['model'];
    pk = json['pk'];
    fields = json['fields'];
  }
}

class RateApp extends StatelessWidget {
  const RateApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Flutter Rating App",
      theme: ThemeData(
        // Define the default brightness and colors.
        brightness: Brightness.dark,
        primaryColor: Colors.lightBlue[800],

        // Define the default font family.
        fontFamily: 'Georgia',

        // Define the default `TextTheme`. Use this to specify the default
        // text styling for headlines, titles, bodies of text, and more.
        textTheme: const TextTheme(
          headline1: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
          headline6: TextStyle(fontSize: 36.0, fontStyle: FontStyle.italic),
          bodyText2: TextStyle(fontSize: 14.0, fontFamily: 'Hind'),
        ),
      ),
      home: const RatePage(title: "STAR RATE"),

    );
  }
}

class RatePage extends StatefulWidget {
  const RatePage({Key? key, required this.title}) : super(key: key);
  final String title;
  @override
  _RatePage createState() => _RatePage();
}

class _RatePage extends State<RatePage> {

  List<RateJson> _rate = <RateJson>[];

  Future<List<RateJson>> fetchNotes() async {

    var url = 'https://kladaf-app.herokuapp.com/rate/json';
    var response = await http.get(Uri.parse(
      'https://kladaf-app.herokuapp.com/rate/json'),
          headers: {
        "Accept":
            "application/json",
            "Access-Control-Allow-Origin": "+"
          });
    print(response);
    // ignore: deprecated_member_use
    var rate = <RateJson>[];
    // if (response.statusCode == 200) {
    // print("200");
    var ratesJson = json.decode(response.body);
    print(ratesJson);
    for (var rateJson in ratesJson) {
      print(RateJson.fromJson(rateJson));
      rate.add(RateJson.fromJson(rateJson));
    }
    //}
    return rate;
  }

    @override
    void initState() {
      fetchNotes().then((value) {
        setState(() {
          _rate.addAll(value);
        });
      });
      super.initState();
    }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      drawer: MainDrawer(),
      appBar: AppBar(
        title: const Text("Rating Card"),
      ),
      body: Container(child:
        ListView.builder(
          reverse: false,
          itemCount: _rate.length,
          itemBuilder: (context, index) {
            return ListBody(children: <Widget>[
              Card(
                child: Padding(
                  padding: EdgeInsets.all(12),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Column(children: [
                        Row(
                          children: <Widget>[
                            StarDisplay(value: _rate[index].fields["star"])
                          ],
                        ),
                      ]),
                      const SizedBox(height: 5),
                      Text(
                        _rate[index].fields["description"],
                        style: TextStyle(
                          fontSize: 25,
                          color: Colors.white,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: size.height * 0.01)
            ]);
          }),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        backgroundColor: Color.fromRGBO(218, 81, 68, 50),
        foregroundColor: Color(0xFFFFFFFF),
        onPressed: (){
          Navigator.of(context).push(MaterialPageRoute(builder: (context) {
            return RatingsPage(title: "RATE");
          }));
        },
      ),
    );
  }
}

class RatingsPage extends StatefulWidget {
  const RatingsPage({Key? key, required this.title}) : super(key: key);
  final String title;
  @override
  _RatingsPage createState() => _RatingsPage();
}

class _RatingsPage extends State<RatingsPage> {
  final _formKey = GlobalKey<FormState>();
  int _rating = 0;
  String desc = "";
  var now = DateTime.now();


  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
        appBar: AppBar(title: Text("KLADAF RATE")),
        body: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              new Text(
                "Bagaimana menurut anda\ntentang web kami secara\nkeseluruhan?",
                style: TextStyle(fontSize: 24,),
                textAlign: TextAlign.center,
              ),

              SizedBox(height: size.height * 0.03),

              Rating((rating) {
                setState(() {
                  _rating = rating;
                });
              }, 5),

              SizedBox(height: size.height * 0.03),

              Container(
                width: 250.0,
                child: TextFormField(
                  style: TextStyle(
                    fontSize: 15.0,
                    height: 2.0,
                  ),
                  decoration: InputDecoration(
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                    hintText: 'Describe your experience',
                  ),
                  validator: (value){
                    if(value!.isEmpty) {
                      return "type '-' if you dont have any comment";
                    }
                    desc = value;
                    return null;
                  },
                ),
              ),

              SizedBox(height: size.height * 0.03),

              ElevatedButton(
                child: const Text(
                  "POST",
                  style: TextStyle(color: Colors.white),
                ),
                onPressed: () async {
                  if (_formKey.currentState!.validate()) {
                    final response = await http.post(
                        Uri.parse(
                            "https://kladaf-app.herokuapp.com/rate/flutter/"),
                        headers: <String, String>{
                          'Content-Type': 'application/json; charset=UTF-8',
                        },
                        body: jsonEncode(<String, dynamic>{
                          'rating': _rating,
                          'description': desc,
                          'date':
                          '${now.year}-${now.month}-${now.day}T${now.hour}:${now.minute}:${now.second}.${now.millisecondsSinceEpoch}Z',
                        })
                    );
                    print(response);
                    print(response.statusCode);
                    print(_rating);
                    print(desc);
                    print(response.body);
                    print(response.body.runtimeType);
                  } else {
                    print("ga valid");
                  }
                  Navigator.pop(context);
                },
                style: ElevatedButton.styleFrom(
                    primary: Colors.orange,
                    padding: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                    textStyle: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold)),
              ),
            ],
          ),
        ));
  }
}

class PageTwo extends StatefulWidget {
  final int rating;
  final String description;
  const PageTwo(
      {required Key key, required this.rating, required this.description})
      : super(key: key);
//super is used to call the constructor of the base class which is the StatefulWidget here
  @override
  _PageTwoState createState() => _PageTwoState();
}
class _PageTwoState extends State<PageTwo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Page 2'),
        automaticallyImplyLeading: false, //optional: removes the default back arrow
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            'Rate: ${widget.rating} was passed',
            style: TextStyle(fontSize: 24,),
            textAlign: TextAlign.center,
          ),

          Text(
            'Description: ${widget.description}',
            style: TextStyle(fontSize: 24,),
            textAlign: TextAlign.center,
          ),
//${} sign here prints the value of variable inside it.
          RaisedButton(
              color: Colors.grey,
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text('<- Go back'))
        ],
      ),
    );
  }
}

class StarDisplay extends StatelessWidget {
  final int value;
  const StarDisplay({Key? key, this.value = 0})
      : assert(value != null),
        super(key: key);
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,
      children: List.generate(5, (index) {
        if (index < value){
          return Icon(Icons.star, color: Colors.orangeAccent, size: 25,);
        } else {
          return Icon(Icons.star_border_outlined, size: 25,);
          //
          // return Icon(
          //   index < value ? (Icons.star, color: Colors.orange, size: 50,) : Icons.star_border,
          // );
        }
      }),
    );
  }
}
