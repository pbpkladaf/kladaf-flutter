import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:http/http.dart' as http;
import 'package:kladafmobile/screens/home.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../main.dart';
import '../session.dart';
import 'login.dart';

class EditProfilePage extends StatefulWidget {
  const EditProfilePage({Key? key}) : super(key: key);

  @override
  _EditProfilePageState createState() => _EditProfilePageState();
}

Future<bool> setting(Map<String, String> data) async {
  final response = await http.post(
      Uri.parse("https://kladaf-app.herokuapp.com/editprofile/"),
      headers: <String, String>{
        'Content-Type': 'application/json;charset=UTF-8'
      },
      body: jsonEncode(data));
  var res = jsonDecode(response.body);
  if (res['success'] == false) {
    return false;
  }
  return true;
}

@override
class _EditProfilePageState extends State<EditProfilePage> {
  final _UserFormKey = GlobalKey<FormState>(); 

  bool checkedValue = false;
  bool checkboxValue = false;
  File? image;
  // TextEditingController nameController = TextEditingController();
  // TextEditingController hobbyController = TextEditingController();

  TextEditingController _name = TextEditingController();
  TextEditingController _hobby = TextEditingController();

  @override
  void initState() {
    _name.text = (Env.userData['name'] ?? "");
    _hobby.text = (Env.userData['hobby'] ?? "");
    super.initState();
  }

  // bool circular = false;

  Widget build(BuildContext context) {
    // var user;
    return Scaffold(
      backgroundColor: Color(0xFF778da9),
      appBar: AppBar(
          backgroundColor: Color(0xff343434),
          elevation: 1,
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Color(0xFFFFFFFF),
            ),
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) =>
                      Home())); //ntar mesti ke home page
            },
          ),
          //ICON EDIT KHUSUS FOTO AJA
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.edit),
              onPressed: () {},
              color: Colors.black,
            )
          ]),
      body: Form(
        // padding: EdgeInsets.only(left: 16, top: 25, right: 16),
        key: _UserFormKey,
        child: GestureDetector(
          onTap: () {
            FocusScope.of(context).unfocus();
          },
          // key: _formKey,
          // padding: EdgeInsets.only(left: 16, top: 25, right: 16),
          child: ListView(
            children: [
              Text(
                "Edit Profile",
                style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 15,
              ),
              Center(
                child: Stack(
                  children: [
                    Container(
                      width: 130,
                      height: 130,
                      decoration: BoxDecoration(
                          border: Border.all(
                              width: 4,
                              color: Theme.of(context).scaffoldBackgroundColor),
                          boxShadow: [
                            BoxShadow(
                                spreadRadius: 2,
                                blurRadius: 10,
                                color: Colors.black.withOpacity(0.1),
                                offset: Offset(0, 10))
                          ],
                          shape: BoxShape.circle,
                          image: DecorationImage(
                              fit: BoxFit.cover,
                              image: NetworkImage(
                                "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_1280.jpg",
                              ))),
                    ),

                    // Positioned(
                    //     bottom: 0,
                    //     right: 0,
                    //     child: Container(
                    //       height: 40,
                    //       width: 40,
                    //       decoration: BoxDecoration(
                    //         shape: BoxShape.circle,
                    //         border: Border.all(
                    //           width: 4,
                    //           color: Theme.of(context).scaffoldBackgroundColor,
                    //         ),
                    //         color: Color(0xFF0D1B2A),
                    //       ),
                    //       // child: Icon(
                    //       //   Icons.edit,
                    //       //   color: Colors.white,
                    //       // ),
                    //     )),
                  ],
                ),
              ),
              // SizedBox(
              //   height: 35,
              // ),
              // Text(
              //   "Hi, luu",
              //   style: TextStyle(fontWeight: FontWeight.bold, fontSize: 21),
              //   textAlign: TextAlign.center, // ntar make user.name
              // ),
              SizedBox(
                height: 35,
              ),
              nameTextField("Name", "Your Name"),
              hobbyTextField("Hobby", "Your Hobby"),
              SizedBox(
                height: 35,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  RaisedButton(onPressed: () async {
                    final prefs = await SharedPreferences.getInstance();

                    if (_UserFormKey.currentState!.validate()) {
                      // print("changes saved");

                      // ScaffoldMessenger.of(context).showSnackBar(
                      //   const SnackBar(content: Text('Changes Saved')),
                      // );
                      int? userId = prefs.getInt('user_id');
                      Map<String, String> data = {};
                      data['name'] = _name.text;
                      data['hobby'] = _hobby.text;

                      bool result = await setting(data);
                      if (result == true) {
                        await getUserData().then((result) {
                          Env.userData['name'] = result['name'];
                          Env.userData['hobby'] = result['hobby'];
                        });
                      }
                      // };
                      result == true
                          ? Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => Home()),
                            )
                          : showDialog(
                              context: context,
                              builder: (BuildContext context) {
                                return AlertDialog(
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(30.0))),
                                    title: Center(
                                        child: Text("Gagal mengganti profil.",
                                            style: TextStyle(
                                                fontWeight: FontWeight.w500,
                                                color: Colors.pink))),
                                    content: Text(
                                      "Data yang dimasukkan tidak valid.",
                                      textAlign: TextAlign.center,
                                    ),
                                    actions: [
                                      Center(
                                          child: ElevatedButton(
                                        child: Text("OK"),
                                        onPressed: () {
                                          Navigator.of(context).pop();
                                        },
                                        style: ButtonStyle(
                                          backgroundColor:
                                            MaterialStateProperty.resolveWith<Color>(
                                          (Set<MaterialState> states) {
                                              if (states.contains(MaterialState.pressed))
                                                return Colors.blue;
                                              return Colors.purple;
                                            },
                                          ),
                                        ),
                                      ))
                                    ]);
                              },
                            );
                      Navigator.of(context).pushAndRemoveUntil(
                          MaterialPageRoute(
                              builder: (context) => Home()),
                          (Route<dynamic> route) => false);
                    };
                  },
                  color:
                      Color(0xff0d1b2a),
                      padding:
                      EdgeInsets.symmetric(horizontal: 50),
                      elevation:
                      2,
                      shape:
                      RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20)),
                      child:
                      Text(
                        "SAVE",
                        style: TextStyle(
                          fontSize: 14,
                          letterSpacing: 2.2,
                          color: Colors.white,
                        ),
                      )
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget nameTextField(String labelText, String placeholder) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 35.0),
      child: TextFormField(
        decoration: InputDecoration(
            contentPadding: EdgeInsets.only(bottom: 3),
            labelText: labelText,
            labelStyle: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 22,
              ),
            floatingLabelBehavior: FloatingLabelBehavior.always,
            hintText: placeholder,
            hintStyle: TextStyle(
              fontSize: 14,
              color: Colors.grey,
            )),
        validator: (value) {
          if (value!.isEmpty) {
            return 'Nama tidak boleh kosong';
          }
          return null;
        },
        controller: _name,
      ),
    );
  }

  Widget hobbyTextField(String labelText, String placeholder) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 35.0),
      child: TextFormField(
        decoration: InputDecoration(
            contentPadding: EdgeInsets.only(bottom: 3),
            labelText: labelText,
            labelStyle: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 22,
              ),
            floatingLabelBehavior: FloatingLabelBehavior.always,
            hintText: placeholder,
            hintStyle: TextStyle(
              fontSize: 14,
              color: Colors.grey,
            )),
        validator: (value) {
          if (value!.isEmpty) {
            return 'Hobi tidak boleh kosong';
          }
          return null;
        },
        controller: _hobby,
      ),
    );
  }
}
