
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:kladafmobile/screens/home.dart';
import 'package:kladafmobile/screens/users.dart';
import 'package:provider/provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../main.dart';
import '../session.dart';
import 'login.dart';
import '../widgets/background_login.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);
  static const routeName = 'loginApp/register/';

  @override
  _RegisterScreenState createState() => _RegisterScreenState();
}

Future<bool> register(Map<String, String> data) async {
  final response = await http.post(Uri.parse("https://kladaf-app.herokuapp.com/registerflutter/"),
      headers: <String, String>{
        'Content-Type': 'application/json;charset=UTF-8'
      },
      body: jsonEncode(data));
  var res = jsonDecode(response.body);
  if (res['success'] == false) {
    return false;
  }
  int userId = res['user_id'];
  String sessionId = res['session_id'];
  setSession(userId, sessionId);
  return true;
}


class _RegisterScreenState extends State<RegisterScreen>{
  final _registerFormKey = GlobalKey<FormState>();
  TextEditingController emailController = TextEditingController();
  TextEditingController usernameController = TextEditingController();
  TextEditingController password1Controller = TextEditingController();
  TextEditingController password2Controller = TextEditingController();


  bool isPasswordVisible = false;
  void togglePasswordView() {
    setState(() {
      isPasswordVisible = !isPasswordVisible;
    });
  }

  // String email = "";
  // String username = "";
  // String password1 = "";
  // String password2 = "";

  @override
  Widget build(BuildContext context) {
    // final request = context.watch<NetworkRequest>();
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      body: Background(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          // children: <Widget>[
          children: [
            //START
            Form(
                key: _registerFormKey,
                child: Column(children: [
                  Container(
                    alignment: Alignment.centerLeft,
                    padding: EdgeInsets.symmetric(horizontal: 40),
                    child: Text(
                      "REGISTER ACCOUNT",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Color(0xFF415A77),
                          fontSize: 36),
                      textAlign: TextAlign.left,
                    ),
                  ),
                  SizedBox(height: size.height * 0.03),
                  Container(
                    alignment: Alignment.center,
                    margin: EdgeInsets.symmetric(horizontal: 40),
                    child: TextFormField(
                      // onChanged: (String value) {
                      //   username = value;
                      // },
                      decoration: InputDecoration(
                          prefixIcon: Icon(Icons.person),
                          hintText: "Username.."),
                          controller: usernameController,
                      // autovalidateMode: AutovalidateMode.onUserInteraction,
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Username tidak boleh kosong';
                        }
                        return null;
                      },
                    ),
                  ),
                  SizedBox(height: size.height * 0.03),
                  Container(
                    alignment: Alignment.center,
                    margin: EdgeInsets.symmetric(horizontal: 40),
                    child: TextFormField(
                      // onChanged: (String value) {
                      //         email = value;
                      // },
                      decoration: InputDecoration(
                          prefixIcon: Icon(Icons.email), hintText: "Email.."),
                      // autovalidateMode: AutovalidateMode.onUserInteraction,
                      controller: emailController,
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Email tidak boleh kosong';
                        } 
                        return null;
                      },
                    ),
                  ),
                  SizedBox(height: size.height * 0.03),
                  Container(
                    alignment: Alignment.center,
                    margin: EdgeInsets.symmetric(horizontal: 40),
                    child: TextFormField(
                      // onChanged: (String value) {
                      //         password1 = value;
                      //       },
                      obscureText: !isPasswordVisible,
                      decoration: InputDecoration(
                        prefixIcon: Icon(Icons.lock),
                        hintText: "Enter password..",
                        suffixIcon: IconButton(
                          color: const Color.fromRGBO(200, 200, 200, 1),
                          splashRadius: 1,
                          icon: Icon(isPasswordVisible
                                ? Icons.visibility_outlined
                                : Icons.visibility_off_outlined),
                          onPressed: togglePasswordView,
                        ),
                      ),
                      // autovalidateMode: AutovalidateMode.onUserInteraction,
                      controller: password1Controller,
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Password tidak boleh kosong';
                        }
                        return null;
                      },
                    ),
                  ),
                  SizedBox(height: size.height * 0.03),
                  Container(
                    alignment: Alignment.center,
                    margin: EdgeInsets.symmetric(horizontal: 40),
                    child: TextFormField(
                      // onChanged: (String value) {
                      //       password2 = value;
                      //     },
                      obscureText: !isPasswordVisible,
                      decoration: InputDecoration(
                          prefixIcon: Icon(Icons.lock),
                          hintText: "Confirm password..",
                          suffixIcon: IconButton(
                          color: const Color.fromRGBO(200, 200, 200, 1),
                          splashRadius: 1,
                          icon: Icon(isPasswordVisible
                                ? Icons.visibility_outlined
                                : Icons.visibility_off_outlined),
                          onPressed: togglePasswordView,
                        ),
                      ),
                      // autovalidateMode: AutovalidateMode.onUserInteraction,
                      controller: password2Controller,
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Password tidak boleh kosong';
                        }
                        return null;
                      },
                      // obscureText: true,
                    ),
                  ),
                  SizedBox(height: size.height * 0.05),
                  Container(
                    alignment: Alignment.center,
                    margin: EdgeInsets.symmetric(horizontal: 40, vertical: 10),
                    child: RaisedButton(
                      onPressed: () async {
                        // if (_registerFormKey.currentState!.validate()) {
                        //   final response = await request.post(
                        //           "http://127.0.0.1:8000/loginApp/register/", //url register
                        //           jsonEncode(<String, String>{
                        //             'username': username,
                        //             'email': email,
                        //             'password1': password1,
                        //             'password2': password2,
                        //           }));
                        //       if (response['status'] == 'success') {
                        //         ScaffoldMessenger.of(context)
                        //             .showSnackBar(const SnackBar(
                        //           content: Text("Account has been successfully registered!"),
                        //         ));

                        //         Navigator.pushReplacement( // PERGI KE USER PROFILE DULU SEMENTARA BARU NTAR GANTI KE HOMEPAGE
                        //         context,
                        //         MaterialPageRoute(
                        //           builder: (context) => const LoginScreen()));
                        //       } else {
                        //         ScaffoldMessenger.of(context)
                        //             .showSnackBar(const SnackBar(
                        //           content: Text(
                        //               "An error occured, please try again."),
                        //         ));
                        //       }
                        // }
                        Map<String, String> data = {};
                          data['username'] = usernameController.text;
                          data['email'] = emailController.text;
                          data['password1'] = password1Controller.text;
                          data['password2'] = password2Controller.text;
                          bool result = await register(data);
                          if (result == true) {
                            await getUserData().then((result) {
                              Env.userData['name'] = result['name'];
                              Env.userData['hobby'] = result['hobby'];
                            });
                          }
                          result == true
                              ? 
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Home()), //hrsnya login
                                )
                              : showDialog(
                                // print("gagal"),
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    shape: RoundedRectangleBorder(
                                      borderRadius:
                                        BorderRadius.all(Radius.circular(30.0))),
                                    title: Center(child: Text("Registrasi gagal.",
                                    style: TextStyle(fontWeight: FontWeight.w500, color: Colors.pink))),
                                    content: Text(
                                        "Data registrasi tidak valid.", textAlign: TextAlign.center,),
                                    actions: [
                                      Center(
                                        child: ElevatedButton(
                                          child: Text("OK"),
                                          onPressed: () {
                                            emailController.text = "";
                                            usernameController.text = "";
                                            password1Controller.text = "";
                                            password2Controller.text = "";
                                            Navigator.of(context).pop();
                                          },
                                          style: ButtonStyle(
                                            backgroundColor:
                                                MaterialStateProperty.resolveWith<Color>(
                                              (Set<MaterialState> states) {
                                                if (states.contains(MaterialState.pressed))
                                                  return Colors.blue;
                                                return Colors.purple;
                                              },
                                            ),
                                          ),
                                        )
                                      )
                                    ]
                                  );
                                },
                              );

                      }, //-----TAMBAHIN AKSI-----
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(80.0)),
                      textColor: Colors.white,
                      padding: const EdgeInsets.all(0),
                      child: Container(
                        alignment: Alignment.center,
                        height: 50.0,
                        width: size.width * 0.5,
                        decoration: new BoxDecoration(
                            borderRadius: BorderRadius.circular(80.0),
                            gradient: new LinearGradient(colors: [
                              Color.fromARGB(255, 13, 27, 42),
                              Color.fromARGB(255, 13, 25, 45)
                            ])),
                        padding: const EdgeInsets.all(0),
                        child: Text(
                          "Create your account",
                          textAlign: TextAlign.center,
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    alignment: Alignment.center,
                    margin: EdgeInsets.symmetric(horizontal: 40, vertical: 10),
                    child: GestureDetector(
                      onTap: () => {
                        Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (context) => LoginScreen()))
                      },
                      child: Text(
                        "Already Have an Account? Login",
                        style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.bold,
                            color: Color(0xFF415A77)),
                      ),
                    ),
                  )
                ]))
          ],
        ),
      ),
    );
  }
}
