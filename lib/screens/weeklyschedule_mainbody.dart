import 'package:flutter/material.dart';
import '../models/weeklyschedule.dart';
import 'weeklyschedule_constants.dart';
import 'weeklyschedule_main.dart';
import 'weeklyschedule_body.dart';
import '../widgets/weeklyschedule_statusbutton.dart';
import '../widgets/navbar.dart';
import '../models/weeklyschedule.dart';
import 'package:http/http.dart' as http;

class DailyScreen extends StatefulWidget {
  const DailyScreen({Key? key}) : super(key: key);

  @override
  _TaskScreenState createState() => _TaskScreenState();
}

class _TaskScreenState extends State<DailyScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff0d1b2a),
      drawer: MainDrawer(),
      body: SafeArea(
        child: Column(
          children: [
            Container(
                alignment: Alignment.center,
                padding: EdgeInsets.fromLTRB(30, 60, 30, 30),
                child: Text(
                  'Weekly Schedule',
                  style: TextStyle(
                      fontFamily: 'Segoe UI',
                      color: Color(0xFFe0e1dd),
                      fontWeight: FontWeight.w500,
                      fontSize: 30),
                )),
            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.fromLTRB(0, 12, 0, 0),
              padding: EdgeInsets.fromLTRB(30, 30, 30, 30),
              width: 200.0,
              decoration: BoxDecoration(
                  color: Color(0xFF415a77),
                  borderRadius: BorderRadius.only(
                    topRight: Radius.circular(40.0),
                    bottomRight: Radius.circular(40.0),
                    topLeft: Radius.circular(40.0),
                    bottomLeft: Radius.circular(40.0),
                  )),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  StatusButton(
                    stat: status[0],
                  ),
                  StatusButton(
                    stat: status[1],
                  ),
                  StatusButton(
                    stat: status[2],
                  ),
                  StatusButton(
                    stat: status[3],
                  ),
                  StatusButton(
                    stat: status[4],
                  ),
                  StatusButton(
                    stat: status[5],
                  ),
                  StatusButton(
                    stat: status[6],
                  ),
                ],
              ),
            ),
            Container(
                padding: EdgeInsets.fromLTRB(0, 30, 0, 0),
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    onPrimary: Colors.white,
                    primary: Color(0xFF778da9),
                  ),
                  child: Text(
                    'Add Schedule',
                    style: TextStyle(color: Color(0xFFe0e1dd), fontSize: 15),
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => MainBody()),
                    );
                  },
                ))
          ],
        ),
      ),
    );
  }
}
