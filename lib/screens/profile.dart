
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:http/http.dart' as http;
import 'package:kladafmobile/screens/users.dart';
import 'package:kladafmobile/widgets/button_widget.dart';
import 'package:kladafmobile/widgets/profile_widget.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../main.dart';
import 'home.dart';
import 'login.dart';

// ini dibuat stateful jg
class ProfilePage extends StatelessWidget {
  final String name;
  final String urlImage;
  final _name = Env.userData['name'];
  final hobby = Env.userData['hobby'];

  ProfilePage({
      Key? key,
      required this.name,
      required this.urlImage,
    }) : super(key: key);


  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
          backgroundColor: Color(0xff343434),
          elevation: 1,
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back,
              color: Color(0xFFFFFFFF),
            ),
            onPressed: () {
              Navigator.of(context).push(MaterialPageRoute(
                  builder: (BuildContext context) => Home())); //ntar mesti ke home page
            },
          ),
        ),
        
      // );
      
      body: ListView(
        physics: BouncingScrollPhysics(),
        children: [
          ProfileWidget(
            imagePath: "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_1280.jpg",
            onClicked: () async {},
          ),
          const SizedBox(height: 24),
          buildName(),
          const SizedBox(height: 24),
          Center(
            child: 
            ButtonWidget(
              text: 'Edit Profile',
              onClicked: () {Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => EditProfilePage()));
                },
              ),
          ),
        ],
      )
    );
  }

  Widget buildName() => Column(
        children: [
          Text(
            (_name ?? "-"),
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
          ),
          const SizedBox(height: 4),
          Text(
            (hobby ?? "-"),
            style: TextStyle(color: Colors.grey),
          )
        ],
      );
  }