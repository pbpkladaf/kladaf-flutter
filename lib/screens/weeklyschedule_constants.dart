import 'package:flutter/cupertino.dart';

List<String> status = [
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday',
  'Sunday',
];

var statusMap = {
  status[0]: 'Monday',
  status[1]: 'Tuesday',
  status[2]: 'Wednesday',
  status[3]: 'Thursday',
  status[4]: 'Friday',
  status[5]: 'Saturday',
  status[6]: 'Sunday',
};

Decoration StatusButtonDecor = BoxDecoration(
    borderRadius: BorderRadius.circular(15.0), color: Color(0xFFe0e1dd));
