import 'package:http/http.dart' as http;
import 'package:kladafmobile/screens/home.dart';
import 'package:kladafmobile/screens/profile.dart';
import 'dart:convert';
import 'package:provider/provider.dart';
// import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import '../main.dart';

import '../session.dart';
import '../widgets/background_login.dart';
import 'users.dart';
import 'register.dart';

class LoginScreen extends StatefulWidget {
  // static const routeName = 'loginApp/login/';

  // const LoginScreen({Key? key}) : super(key: key);
  @override
  _LoginScreenState createState() => _LoginScreenState();

}

Future<bool> login(String username, String password) async {
  final response = await http.post(Uri.parse("https://kladaf-app.herokuapp.com/loginflutter/"),
      headers: <String, String>{
        'Content-Type': 'application/json;charset=UTF-8'
      },
      body: jsonEncode(<String, dynamic>{
        'username': username,
        'password': password,
      }));
  var res = jsonDecode(response.body);
  if (res['success'] == false) {
    return false;
  }
  int userId = res['user_id'];
  String sessionId = res['session_id'];
  setSession(userId, sessionId);
  return true;
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController usernameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  bool isPasswordVisible = false;
  void togglePasswordView() {
    setState(() {
      isPasswordVisible = !isPasswordVisible;
    });
  }

  // String username = "";
  // String password1 = "";

  @override
  Widget build(BuildContext context) {
    // final request = context.watch<NetworkRequest>();

    Size size = MediaQuery.of(context).size;

    return Scaffold(
      body: Background(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          // children: <Widget>[
          children: [
            //START
            Form(
                // key: _loginFormKey,
                child: Column(children: [
                  //END
                  Container(
                    alignment: Alignment.centerLeft,
                    padding: EdgeInsets.symmetric(horizontal: 40),
                    child: Text(
                      "LOGIN",
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Color(0xFF415A77),
                          fontSize: 36),
                      textAlign: TextAlign.left,
                    ),
                  ),
                  SizedBox(height: size.height * 0.03),
                  Container(
                    alignment: Alignment.center,
                    margin: EdgeInsets.symmetric(horizontal: 40),
                    child: TextFormField(
                      // onChanged: (String value) {
                      //   username = value;
                      // },
                      decoration: InputDecoration(
                          prefixIcon: Icon(Icons.person),
                          hintText: "Username.."),
                      // autovalidateMode: AutovalidateMode.onUserInteraction,
                      controller: usernameController,
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Username tidak boleh kosong';
                        }
                        return null;
                      },
                    ),
                  ),
                  SizedBox(height: size.height * 0.03),
                  Container(
                    alignment: Alignment.center,
                    margin: EdgeInsets.symmetric(horizontal: 40),
                    child: TextFormField(
                      // onChanged: (String value) {
                      //   password1 = value;
                      // },
                      obscureText: !isPasswordVisible,
                      decoration: InputDecoration(
                        prefixIcon: Icon(Icons.lock),
                        hintText: "Password..",
                        suffixIcon: IconButton(
                          color: const Color.fromRGBO(200, 200, 200, 1),
                          splashRadius: 1,
                          icon: Icon(isPasswordVisible
                              ? Icons.visibility_outlined
                              : Icons.visibility_off_outlined),
                          onPressed: togglePasswordView,
                        ),
                      ),
                      // autovalidateMode: AutovalidateMode.onUserInteraction,
                      controller: passwordController,
                      validator: (value) {
                        if (value!.isEmpty) {
                          return 'Password tidak boleh kosong';
                        }
                        return null;
                      },
                      // obscureText: true,
                    ),
                  ),
                  SizedBox(height: size.height * 0.05),
                  Container(
                    alignment: Alignment.center,
                    margin: EdgeInsets.symmetric(horizontal: 40, vertical: 10),
                    child: RaisedButton(
                      onPressed: () async {
                        // if (_loginFormKey.currentState!.validate()) {
                        //   final response = await request
                        //   .login("http://127.0.0.1:8000/loginApp/loginflutter/", {
                        //     'username': username,
                        //     'password': password1,
                        //   });

                          // if (response['status']) {
                          //     ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                          //       content: Text("Successfully logged in!"),
                          //     ));

                          //     // Navigator.pushNamed(context, "/");// ini mau ke home page maksudnya
                          //     Navigator.pushReplacement( // PERGI KE USER PROFILE DULU SEMENTARA BARU NTAR GANTI KE HOMEPAGE
                          //     context,
                          //     MaterialPageRoute(
                          //         builder: (context) => const EditProfilePage())); 
                          //   } else {
                          //     ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                          //       content: Text("An error occured, please try again."),
                          //     ));
                          //   }

                          // if (request.loggedIn) {
                          //     Navigator.push(
                          //      context,
                          //      MaterialPageRoute(
                          //          builder: (context) => EditProfilePage()));
                          //   } else {
                          //     _showErrors(context, response);
                          //   }
                          String username = usernameController.text;
                          String password = passwordController.text;
                          bool result = await login(username, password);
                          if (result == true) {
                            await getUserData().then((result) {
                              Env.userData['name'] = result['name'];
                              Env.userData['hobby'] = result['hobby'];
                            });
                          }
                          result == true
                              ? (Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Home()),
                                ))
                              : showDialog(
                                context: context,
                                builder: (BuildContext context) {
                                  return AlertDialog(
                                    shape: RoundedRectangleBorder(
                                      borderRadius:
                                        BorderRadius.all(Radius.circular(30.0))),
                                    title: Center(child: Text("Login gagal.",
                                    style: TextStyle(fontWeight: FontWeight.w500, color: Colors.red))),
                                    content: Text(
                                        "Username atau Password salah.", textAlign: TextAlign.center,),
                                    actions: [
                                      Center(
                                        child: ElevatedButton(
                                          child: Text("OK"),
                                          onPressed: () {
                                            usernameController.text = "";
                                            passwordController.text = "";
                                            Navigator.of(context).pop();
                                          },
                                          style: ButtonStyle(
                                            backgroundColor:
                                                MaterialStateProperty.resolveWith<Color>(
                                              (Set<MaterialState> states) {
                                                if (states.contains(MaterialState.pressed))
                                                  return Colors.blue;
                                                return Colors.black;
                                              },
                                            ),
                                          ),
                                        )
                                      )
                                    ]
                                  );
                                },
                              );
                        // }),
                  // ),

                      },
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(80.0)),
                      textColor: Colors.white,
                      padding: const EdgeInsets.all(0),
                      child: Container(
                        alignment: Alignment.center,
                        height: 50.0,
                        width: size.width * 0.5,
                        decoration: new BoxDecoration(
                            borderRadius: BorderRadius.circular(80.0),
                            gradient: new LinearGradient(colors: [
                              Color.fromARGB(255, 13, 27, 42),
                              Color.fromARGB(255, 13, 25, 45)
                            ])),
                        padding: const EdgeInsets.all(0),
                        child: const Text(
                          "LOGIN",
                          textAlign: TextAlign.center,
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    alignment: Alignment.center,
                    margin: EdgeInsets.symmetric(horizontal: 40, vertical: 10),
                    child: GestureDetector(
                      onTap: () => {
                        // Navigator.pushNamed(context, )
                        Navigator.pushReplacement(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const RegisterScreen()))
                      },
                      child: Text(
                        "Don't Have an Account? Sign up",
                        style: TextStyle(
                            fontSize: 12,
                            fontWeight: FontWeight.bold,
                            color: Color(0xFF415A77)),
                      ),
                    ),
                  )
                ]))
          ],
        ),
      ),
    );
  }
}
