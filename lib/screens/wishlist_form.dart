import 'package:flutter/material.dart';
import 'package:kladafmobile/api/wishlist_api.dart';
import 'package:kladafmobile/main.dart';
import 'package:kladafmobile/models/wishlist.dart';
import 'package:kladafmobile/screens/home.dart';
import 'package:provider/provider.dart';

class AddNewWish extends StatelessWidget {
  const AddNewWish({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => WishListProvider(),
      child: const MaterialApp(
        title: 'Flutter Demo',
        home: AddNewWishPage(),
      ),
    );
  }
}

class AddNewWishPage extends StatefulWidget {
  const AddNewWishPage({Key? key}) : super(key: key);

  @override
  _AddNewWishPageState createState() => _AddNewWishPageState();
}

class _AddNewWishPageState extends State<AddNewWishPage>{
  final textController = TextEditingController();

  void onAdd() {
    final String textVal = textController.text;
    if (textVal.isNotEmpty){
      final WishList wish = WishList(id: DateTime.now().hashCode,text: textVal, complete: false);
      Provider.of<WishListProvider>(context, listen: false).addWishList(wish);
    }
  }

  @override
  Widget build(BuildContext context){
    void backHome() {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => const Home()),
      );
    }

    return MaterialApp(
        home: Scaffold(
          backgroundColor: Color(0xFF0D1B2A),
          appBar: AppBar(
            title: Text(
              'Wish List', style: TextStyle(color: Color(0xFF0D1B2A), fontSize: 26),
            ),
            backgroundColor: Colors.white,
            centerTitle: true,
            iconTheme: IconThemeData(color: Color(0xFF0D1B2A)),
          ),
          drawer: Drawer(
            child: ListView(
              padding: EdgeInsets.zero,
              children: const <Widget>[
                DrawerHeader(
                  decoration: BoxDecoration(
                    color: Color(0xFF0D1B2A),
                  ),
                  child: Text(
                    'User Profile', style: TextStyle(color: Colors.blueGrey, fontSize: 24),
                  ),
                ),
                ListTile(
                  title: Text('Diary'),
                ),
                ListTile(
                  title: Text('Podomoro Timer'),
                ),
                ListTile(
                  title: Text('Weekly Schedule'),
                ),
                ListTile(
                  title: Text('Proggres Tracker'),
                ),
                ListTile(
                  title: Text(
                    'Wish List', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
                  ),
                ),
                ListTile(
                  title: Text('Rating Kladaf App'),
                ),
                ListTile(
                  title: Text('Rating Kladaf App'),
                ),
              ],
            ),
          ),
          //bodyyy ya nanti
          body: ListView(
            children: [
              Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      child: Text('                 ', style: TextStyle(fontSize: 50),),
                    ),
                    TextField(
                      decoration: InputDecoration(
                          fillColor: Colors.white.withOpacity(1),
                          filled: true,
                          border: const OutlineInputBorder(
                            borderRadius: BorderRadius.all(Radius.circular(8.0),),
                            borderSide: BorderSide(color: Colors.grey, width: 8.0,),),
                          labelText: 'Add your wish list',
                          labelStyle: TextStyle(color: Colors.blueGrey.withOpacity(0.6),)),
                      controller: textController,
                    ),
                    Container(
                      child: Text('                 ', style: TextStyle(fontSize: 20),),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 170),
                      child: ElevatedButton(
                        onPressed: () {
                          onAdd();
                        },
                        child: const Text('ADD', style: TextStyle(fontSize: 15, fontWeight: FontWeight.bold,),),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ));
  }
}