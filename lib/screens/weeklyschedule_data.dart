import 'package:flutter/material.dart';
import '../widgets/navbar.dart';
import 'weeklyschedule_body.dart';
import 'weeklyschedule_constants.dart';
import '../models/weeklyschedule.dart';
import 'package:http/http.dart' as http;

class FetchData extends StatefulWidget {
  const FetchData({Key? key}) : super(key: key);

  @override
  _MainDataState createState() => _MainDataState();
}

class _MainDataState extends State<FetchData> {
  List<Schedule>? scheduleList;
  var hourList = <TextEditingController>[];
  var activityList = <TextEditingController>[];
  var detailList = <TextEditingController>[];
  var messageList = <TextEditingController>[];

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addPostFrameCallback((_) {
      getSectionData(context);
    });
  }

  Future<void> getSectionData(BuildContext context) async {
    var response = await http.get(
        Uri.parse(
            "https://kladaf-app.herokuapp.com/weekly-schedule/all-schedule-api/"),
        headers: {
          "Accept": "aplication/json",
        });
    print(response.body);
    if (response.statusCode == 200) {
      setState(() {
        print('success');
        scheduleList = scheduleFromJson(response.body);
      });
    } else {
      print("Error occured");
    }
  }

  Future<void> postSectionData(BuildContext context) async {
    var response = await http.post(
        Uri.parse(
            "https://kladaf-app.herokuapp.com/weekly-schedule/all-schedule-api/"),
        headers: {
          "content-type": "application/json;charset=UTF-8",
        });
    print(response.body);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        drawer: MainDrawer(),
        body: Container(
            color: Color(0xff0d1b2a),
            child: Padding(
                padding: EdgeInsets.fromLTRB(30, 60, 30, 20),
                child: ListView(children: <Widget>[
                  Container(
                      alignment: Alignment.center,
                      padding: EdgeInsets.all(10),
                      child: Column(children: <Widget>[
                        for (var i = 0; i < 24; i++)
                          Card(
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                    BorderRadius.all(Radius.circular(15.0))),
                            elevation: 1.9,
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(15.0),
                                color: Color(0xFF778da9),
                              ),
                              padding: EdgeInsets.all(5.0),
                              child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: <Widget>[
                                    Text(i.toString() + ':00',
                                        style: TextStyle(
                                            color: Color(0xFFe0e1dd),
                                            fontSize: 30,
                                            fontWeight: FontWeight.bold)),
                                    const MySeparator(color: Colors.grey),
                                    Text('Activity: ',
                                        style: TextStyle(
                                            color: Color(0xFFe0e1dd),
                                            fontSize: 20)),
                                    Text('Detail: ',
                                        style: TextStyle(
                                            color: Color(0xFFe0e1dd),
                                            fontSize: 15)),
                                    Text('Message: ',
                                        style: TextStyle(
                                            color: Color(0xFFe0e1dd),
                                            fontSize: 15)),
                                  ]),
                              height: 175.0,
                              width: 350.0,
                            ),
                          )
                      ])),
                  Container(
                    padding: EdgeInsets.fromLTRB(0, 30, 0, 0),
                    child: Center(
                        child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        onPrimary: Colors.white,
                        primary: Color(0xFF415a77),
                      ),
                      child: Text('Back'),
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    )),
                  ),
                ]))));
  }
}

class MySeparator extends StatelessWidget {
  final double height;
  final Color color;

  const MySeparator({this.height = 1, this.color = Colors.black});

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (BuildContext context, BoxConstraints constraints) {
        final boxWidth = constraints.constrainWidth();
        final dashWidth = 10.0;
        final dashHeight = height;
        final dashCount = (boxWidth / (2 * dashWidth)).floor();
        return Flex(
          children: List.generate(dashCount, (_) {
            return SizedBox(
              width: dashWidth,
              height: dashHeight,
              child: DecoratedBox(
                decoration: BoxDecoration(color: color),
              ),
            );
          }),
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          direction: Axis.horizontal,
        );
      },
    );
  }
}
