import 'package:flutter/material.dart';
import '../widgets/navbar.dart';
import '../main.dart';

class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    fetchDiaryData();
    return Scaffold(
        backgroundColor: Color(0xff0d1b2a),
        appBar: AppBar(
          title: Text(
            'Kladaf App',
            style: TextStyle(
              color: Color(0xFFe0e1dd),
              fontFamily: 'Segoe UI',
            ),
          ),
          backgroundColor: Color(0xff343434),
        ),
        drawer: MainDrawer(),
        body: SafeArea(
          child: Column(
            children: <Widget>[
              Container(
                width: 300,
                height: 300,
                alignment: Alignment.center,
                padding: EdgeInsets.fromLTRB(30, 70, 30, 0),
                child: Image.asset('assets/images/productive.png'),
              ),
              Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.fromLTRB(30, 35, 30, 20),
                  child: Text(
                    'Welcome to Kladaf!',
                    style: TextStyle(
                        fontFamily: 'Segoe UI',
                        color: Color(0xFFe0e1dd),
                        fontWeight: FontWeight.w500,
                        fontSize: 36),
                  )),
              Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.fromLTRB(30, 0, 30, 40),
                  child: Text(
                    'We will help you to stay productive',
                    style: TextStyle(
                        fontFamily: 'Segoe UI',
                        color: Color(0xFFe0e1dd),
                        fontWeight: FontWeight.w500,
                        fontSize: 17),
                  )),
              Container(
                  alignment: Alignment.center,
                  padding: EdgeInsets.fromLTRB(30, 20, 30, 30),
                  child: Text(
                    'Want to have the best experience? Click here.',
                    style: TextStyle(
                        fontFamily: 'Segoe UI',
                        color: Colors.white,
                        backgroundColor: Color(0xFF384e67),
                        fontWeight: FontWeight.w500,
                        fontSize: 11),
                  )),
            ],
          ),
        ));
  }
}
