import 'package:flutter/cupertino.dart';

List<String> status = [
  'No Status',
  'Not Started',
  'In Progress',
  'In Review',
  'Completed'
];

var statusMap = {
  status[0]: 'No Status',
  status[1]: 'Not Started',
  status[2]: 'In Progress',
  status[3]: 'In Review',
  status[4]: 'Completed'
};

Decoration StatusButtonDecor = BoxDecoration(
    borderRadius: BorderRadius.circular(15.0), color: Color(0xFFe0e1dd));
