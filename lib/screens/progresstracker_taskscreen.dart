import 'package:flutter/material.dart';
import 'package:kladafmobile/widgets/navbar.dart';
import 'progresstracker_form.dart';
import '../widgets/progresstracker_statusbutton.dart';
import 'progresstracker_home.dart';
import 'progresstracker_constants.dart';

class TaskScreen extends StatefulWidget {
  const TaskScreen({Key? key}) : super(key: key);

  @override
  _TaskScreenState createState() => _TaskScreenState();
}

class _TaskScreenState extends State<TaskScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff0d1b2a),
      body: SafeArea(
        child: Row(children: <Widget>[
          Container(
            height: double.infinity,
            width: 100.0,
            decoration: BoxDecoration(
                color: Color(0xFF415a77),
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(40.0),
                  bottomRight: Radius.circular(40.0),
                )),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                StatusButton(
                  stat: status[0],
                ),
                StatusButton(
                  stat: status[1],
                ),
                StatusButton(
                  stat: status[2],
                ),
                StatusButton(
                  stat: status[3],
                ),
                StatusButton(
                  stat: status[4],
                ),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    onPrimary: Colors.white,
                    primary: Color(0xFF0d1b2a),
                  ),
                  child: Text(
                    'Add',
                    style: TextStyle(color: Color(0xFFe0e1dd), fontSize: 24),
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => const FormPage()),
                    );
                  },
                )
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(15.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  'Progress Tracker',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Color(0xFF778da9),
                    fontSize: 32.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          )
        ]),
      ),
      drawer: MainDrawer(),
    );
  }
}
