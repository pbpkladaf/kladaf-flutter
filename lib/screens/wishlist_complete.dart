import 'package:kladafmobile/api/wishlist_api.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class WishListDone extends StatelessWidget {
  const WishListDone({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => WishListProvider(),
      child: const MaterialApp(
        title: 'Flutter Demo',
        home: WishListDonePage(),
      ),
    );
  }
}

class WishListDonePage extends StatefulWidget {
  const WishListDonePage({Key? key}) : super(key: key);

  @override
  _WishListDonePageState createState() => _WishListDonePageState();
}

class _WishListDonePageState extends State<WishListDonePage> {
  Widget build(BuildContext context) {
    final wishP = Provider.of<WishListProvider>(context);

    return MaterialApp(
        home: Scaffold(
          backgroundColor: Color(0xFF0D1B2A),
          appBar: AppBar(
            title: Text(
              'Wish List', style: TextStyle(color: Color(0xFF0D1B2A), fontSize: 26),
            ),
            backgroundColor: Colors.white,
            centerTitle: true,
            iconTheme: IconThemeData(color: Color(0xFF0D1B2A)),
          ),
          drawer: Drawer(
            child: ListView(
              padding: EdgeInsets.zero,
              children: const <Widget>[
                DrawerHeader(
                  decoration: BoxDecoration(
                    color: Color(0xFF0D1B2A),
                  ),
                  child: Text(
                    'User Profile', style: TextStyle(color: Colors.blueGrey, fontSize: 24),
                  ),
                ),
                ListTile(
                  title: Text('Diary'),
                ),
                ListTile(
                  title: Text('Podomoro Timer'),
                ),
                ListTile(
                  title: Text('Weekly Schedule'),
                ),
                ListTile(
                  title: Text('Proggres Tracker'),
                ),
                ListTile(
                  title: Text(
                    'Wish List', style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
                  ),
                ),
                ListTile(
                  title: Text('Rating Kladaf App'),
                ),
                ListTile(
                  title: Text('Rating Kladaf App'),
                ),
              ],
            ),
          ),
          //bodyyy ya nanti
          body: ListView.builder(
            shrinkWrap: true,
            itemCount: wishP.complete.length,
            itemBuilder: (BuildContext context, int index) {
              return ListTile(
                title: Text(
                  wishP.complete[index].text,
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white),
                ),
              );
            },
          ),
        ));
  }
}