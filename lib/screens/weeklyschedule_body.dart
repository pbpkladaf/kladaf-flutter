import 'package:flutter/material.dart';
import 'weeklyschedule_main.dart';
import '../widgets/navbar.dart';
import '../models/weeklyschedule.dart';
import 'package:http/http.dart' as http;

class MainBody extends StatefulWidget {
  @override
  State<MainBody> createState() => _MainBodyState();
}

class _MainBodyState extends State<MainBody> {
  TextEditingController activityController = TextEditingController();
  TextEditingController timeController = TextEditingController();
  TextEditingController detailController = TextEditingController();
  TextEditingController messageController = TextEditingController();
  List<bool> days = [false, false, false, false, false, false, false];
  List<List<TextEditingController>>? schedules;

  Future<void> getSectionData(
      TextEditingController activity,
      TextEditingController time,
      TextEditingController detail,
      TextEditingController message) async {
    var response = await http.get(
        Uri.parse(
            "https://kladaf-app.herokuapp.com/weekly-schedule/all-schedule-api/"),
        headers: {
          "Accept": "aplication/json",
        });
    print(response.body);
    if (response.statusCode == 200) {
      setState(() {
        print('success');
        schedules?.add([activity, time, detail, message]);
      });
    } else {
      print("Error occured");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: MainDrawer(),
      body: Container(
          color: Color(0xff0d1b2a),
          child: Padding(
              padding: EdgeInsets.fromLTRB(30, 60, 30, 20),
              child: ListView(children: <Widget>[
                Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.all(10),
                    child: Text(
                      'Add Weekly Schedule',
                      style: TextStyle(
                          fontFamily: 'Segoe UI',
                          color: Color(0xFFe0e1dd),
                          fontWeight: FontWeight.w500,
                          fontSize: 30),
                    )),
                Container(
                    alignment: Alignment.center,
                    padding: EdgeInsets.fromLTRB(30, 30, 30, 20),
                    child: Text(
                      'Fill The Information',
                      style: TextStyle(
                          fontFamily: 'Segoe UI',
                          color: Color(0xFFe0e1dd),
                          fontSize: 20),
                    )),
                Container(
                    padding: EdgeInsets.fromLTRB(140, 10, 140, 10),
                    child: TextField(
                      controller: activityController,
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: Color(0xFF748ba5),
                          contentPadding: EdgeInsets.all(1.0),
                          border: OutlineInputBorder(),
                          labelText: 'Type your Activity here',
                          hintText: ' Ex: Study web development lecture',
                          labelStyle: TextStyle(
                            fontFamily: 'Segoe UI',
                            color: Color(0xFFadb5bd),
                          )),
                    )),
                Container(
                    padding: EdgeInsets.fromLTRB(140, 10, 140, 10),
                    child: TextField(
                      controller: timeController,
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: Color(0xFF748ba5),
                          contentPadding: EdgeInsets.all(1.0),
                          border: OutlineInputBorder(),
                          labelText: 'Type your Time here',
                          hintText: ' Ex: 6:00 or 13:00',
                          labelStyle: TextStyle(
                            fontFamily: 'Segoe UI',
                            color: Color(0xFFadb5bd),
                          )),
                    )),
                Container(
                    padding: EdgeInsets.fromLTRB(140, 10, 140, 10),
                    child: TextField(
                      controller: detailController,
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: Color(0xFF748ba5),
                          contentPadding: EdgeInsets.all(1.0),
                          border: OutlineInputBorder(),
                          labelText: 'Type your Detail here',
                          hintText:
                              ' Ex: Try to solve 25 problems in 1 hour without looking on the internet',
                          labelStyle: TextStyle(
                            fontFamily: 'Segoe UI',
                            color: Color(0xFFadb5bd),
                          )),
                    )),
                Container(
                    padding: EdgeInsets.fromLTRB(140, 10, 140, 10),
                    child: TextField(
                      controller: messageController,
                      decoration: InputDecoration(
                          filled: true,
                          fillColor: Color(0xFF748ba5),
                          contentPadding: EdgeInsets.all(1.0),
                          border: OutlineInputBorder(),
                          labelText: 'Type your Message here',
                          hintText: ' Ex: Always pray before study',
                          labelStyle: TextStyle(
                            fontFamily: 'Segoe UI',
                            color: Color(0xFFadb5bd),
                          )),
                    )),
                Theme(
                  data: ThemeData(unselectedWidgetColor: Color(0xFF6096ba)),
                  child: Container(
                      padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          // [Monday] checkbox
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Checkbox(
                                checkColor: Color(0xFFd9d9d9),
                                value: days[0],
                                onChanged: (bool? value) {
                                  setState(() {
                                    days[0] = !days[0];
                                  });
                                },
                              ),
                              Text(
                                "Mon",
                                style: TextStyle(color: Color(0xFFd9d9d9)),
                              ),
                            ],
                          ),
                          // [Tuesday] checkbox
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Checkbox(
                                checkColor: Color(0xFFd9d9d9),
                                value: days[1],
                                onChanged: (bool? value) {
                                  setState(() {
                                    days[1] = !days[1];
                                  });
                                },
                              ),
                              Text(
                                "Tue",
                                style: TextStyle(color: Color(0xFFd9d9d9)),
                              ),
                            ],
                          ),
                          // [Wednesday] checkbox
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Checkbox(
                                checkColor: Color(0xFFd9d9d9),
                                value: days[2],
                                onChanged: (bool? value) {
                                  setState(() {
                                    days[2] = !days[2];
                                  });
                                },
                              ),
                              Text(
                                "Wed",
                                style: TextStyle(color: Color(0xFFd9d9d9)),
                              ),
                            ],
                          ),
                          // [Thursday] checkbox
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Checkbox(
                                checkColor: Color(0xFFd9d9d9),
                                value: days[3],
                                onChanged: (bool? value) {
                                  setState(() {
                                    days[3] = !days[3];
                                  });
                                },
                              ),
                              Text(
                                "Thur",
                                style: TextStyle(color: Color(0xFFd9d9d9)),
                              ),
                            ],
                          ),
                          // [Friday] checkbox
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Checkbox(
                                checkColor: Color(0xFFd9d9d9),
                                value: days[4],
                                onChanged: (bool? value) {
                                  setState(() {
                                    days[4] = !days[4];
                                  });
                                },
                              ),
                              Text(
                                "Fri",
                                style: TextStyle(color: Color(0xFFd9d9d9)),
                              ),
                            ],
                          ),
                          // [Saturday] checkbox
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Checkbox(
                                checkColor: Color(0xFFd9d9d9),
                                value: days[5],
                                onChanged: (bool? value) {
                                  setState(() {
                                    days[5] = !days[5];
                                  });
                                },
                              ),
                              Text(
                                "Sat",
                                style: TextStyle(color: Color(0xFFd9d9d9)),
                              ),
                            ],
                          ),
                          // [Sunday] checkbox
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Checkbox(
                                checkColor: Color(0xFFd9d9d9),
                                value: days[6],
                                onChanged: (bool? value) {
                                  setState(() {
                                    days[6] = !days[6];
                                  });
                                },
                              ),
                              Text(
                                "Sun",
                                style: TextStyle(color: Color(0xFFd9d9d9)),
                              ),
                            ],
                          ),
                        ],
                      )),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(0, 30, 0, 0),
                  child: Center(
                      child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      onPrimary: Colors.white,
                      primary: Color(0xFF415a77),
                    ),
                    child: Text('Add Schedule'),
                    onPressed: () {
                      getSectionData(activityController, timeController,
                          detailController, messageController);
                    },
                  )),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(0, 30, 0, 0),
                  child: Center(
                      child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      onPrimary: Colors.white,
                      primary: Color(0xFF415a77),
                    ),
                    child: Text('Back'),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  )),
                ),
              ]))),
    );
  }
}
