import 'package:kladafmobile/api/wishlist_api.dart';
import 'package:flutter/material.dart';
import 'package:kladafmobile/screens/wishlist_complete.dart';
import 'package:kladafmobile/screens/wishlist_form.dart';
import 'package:kladafmobile/screens/wishlist_list.dart';
import 'package:provider/provider.dart';

// void main() {
//   runApp(MyApp());
// }

class WishListApp extends StatelessWidget {
  const WishListApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => WishListProvider(),
      child: const MaterialApp(
        title: 'Flutter Demo',
        home: HomePage(),
      ),
    );
  }
}

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _currentIndex = 0;
  final List<Widget> _children = [
    const WishListHome(),
    const WishListDone(),
    const AddNewWish(),
  ];

  void onTappedBar(int index){
    setState(() {
      _currentIndex = index;
    });
  }

  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
          backgroundColor: Color(0xFF0D1B2A),
          //bodyyy ya nanti
          body: _children[_currentIndex],
          bottomNavigationBar: BottomNavigationBar(
            onTap: onTappedBar,
            currentIndex: _currentIndex,
            items: const <BottomNavigationBarItem>[
              BottomNavigationBarItem(
                icon: Icon(Icons.list),
                label: 'My Wish',
              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.check),
                label: 'Done',

              ),
              BottomNavigationBarItem(
                icon: Icon(Icons.add),
                label: 'Add New',
              ),
            ],
            selectedItemColor: Color(0xFF0D1B2A),
            unselectedItemColor: Colors.grey,
            showUnselectedLabels: true,
          ),
        ));
  }
}