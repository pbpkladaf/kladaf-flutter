// To parse this JSON data, do
//
//     final task = taskFromJson(jsonString);

import 'dart:convert';

List<Task> taskFromJson(String str) =>
    List<Task>.from(json.decode(str).map((x) => Task.fromJson(x)));

String taskToJson(List<Task> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Task {
  Task({
    this.model,
    this.pk,
    this.fields,
  });

  String? model;
  int? pk;
  Fields? fields;

  factory Task.fromJson(Map<String, dynamic> json) => Task(
        model: json["model"],
        pk: json["pk"],
        fields: Fields.fromJson(json["fields"]),
      );

  Map<String, dynamic> toJson() => {
        "model": model,
        "pk": pk,
        "fields": fields!.toJson(),
      };
}

class Fields {
  Fields({
    this.addTasks,
    this.status,
    this.user,
  });

  String? addTasks;
  String? status;
  int? user;

  factory Fields.fromJson(Map<String, dynamic> json) => Fields(
        addTasks: json["add_tasks"],
        status: json["status"],
        user: json["user"],
      );

  Map<String, dynamic> toJson() => {
        "add_tasks": addTasks,
        "status": status,
        "user": user,
      };
}
