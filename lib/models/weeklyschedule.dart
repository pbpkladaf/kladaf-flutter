import 'package:flutter/material.dart';

// To parse this JSON data, do
// from https://app.quicktype.io/
//     final schedule = scheduleFromJson(jsonString);

import 'dart:convert';

List<Schedule> scheduleFromJson(String str) =>
    List<Schedule>.from(json.decode(str).map((x) => Schedule.fromJson(x)));

String scheduleToJson(List<Schedule> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Schedule {
  Schedule({
    this.model,
    this.pk,
    this.fields,
  });

  String? model;
  int? pk;
  Fields? fields;

  factory Schedule.fromJson(Map<String, dynamic> json) => Schedule(
        model: json["model"],
        pk: json["pk"],
        fields: Fields.fromJson(json["fields"]),
      );

  Map<String, dynamic> toJson() => {
        "model": model,
        "pk": pk,
        "fields": fields!.toJson(),
      };
}

class Fields {
  Fields({
    this.monday,
    this.tuesday,
    this.wednesday,
    this.thursday,
    this.friday,
    this.saturday,
    this.sunday,
    this.activity,
    this.hour,
    this.detail,
    this.message,
    this.user,
  });

  bool? monday;
  bool? tuesday;
  bool? wednesday;
  bool? thursday;
  bool? friday;
  bool? saturday;
  bool? sunday;
  String? activity;
  String? hour;
  String? detail;
  String? message;
  int? user;

  factory Fields.fromJson(Map<String, dynamic> json) => Fields(
        monday: json["monday"],
        tuesday: json["tuesday"],
        wednesday: json["wednesday"],
        thursday: json["thursday"],
        friday: json["friday"],
        saturday: json["saturday"],
        sunday: json["sunday"],
        activity: json["activity"],
        hour: json["hour"],
        detail: json["detail"],
        message: json["message"],
        user: json["user"],
      );

  Map<String, dynamic> toJson() => {
        "monday": monday,
        "tuesday": tuesday,
        "wednesday": wednesday,
        "thursday": thursday,
        "friday": friday,
        "saturday": saturday,
        "sunday": sunday,
        "activity": activity,
        "hour": hour,
        "detail": detail,
        "message": message,
        "user": user,
      };
}
