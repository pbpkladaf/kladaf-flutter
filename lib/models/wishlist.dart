class WishList {
  int id;
  final String text;
  final bool complete;

  WishList({required this.id, required this.text, required this.complete});

  factory WishList.fromJson(Map<String, dynamic> json){
    return WishList (
      id : json['id'],
      text : json['text'],
      complete : json['complete'],
    );
  }

  dynamic toJson() => {
    'id': id,
    'text': text,
    'complete':complete,
  };
}