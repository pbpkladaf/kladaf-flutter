class Diary {
  final String id;
  final String title;
  final String dateCreated;
  final String content;
  final String userId;

  const Diary({
    required this.id,
    required this.title,
    required this.dateCreated,
    required this.content,
    required this.userId,
  });
}
