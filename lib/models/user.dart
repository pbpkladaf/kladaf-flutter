class User {
  final String imagePath;
  final String name;

  const User({
    required this.imagePath,
    required this.name,
  });
}