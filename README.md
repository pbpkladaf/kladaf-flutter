# Kladaf Flutter


## Anggota Kelompok:
- Alya Muthia Fitri - 2006487591
- Andrea Debora Narulita - 2006530311
- Dyta Dewipuspita - 2006533925
- Dzulfikar Rachman - 2006523086
- Ferdinand Amos Papilaya - 2006536486
- Kezia Putri Yusika - 2006524712
- Lulu Kamilia Sudirman - 2006520771


## Kladaf-app
- Link Heroku Web: https://kladaf-app.herokuapp.com/
- Link Aplikasi : https://ristek.link/kladaf-flutter


## Cerita Aplikasi
- Pandemi Covid-19 membawa dampak yang sangat besar bagi cara hidup manusia. Banyak orang yang diwajibkan untuk melakukan pekerjaannya dari rumah (WFH), bahkan kegiatan pembelajaran pada setiap jenjang pendidikan juga dilakukan dari rumah masing-masing secara online. Tidak sedikit orang yang kesulitan untuk mengatur waktu beraktivitas agar dapat tetap fokus ketika bekerja atau belajar di rumah. Oleh karena itu, kami ingin menciptakan Kladaf sebagai sebuah productivity web-application yang dapat membantu mereka yang mengalami masalah-masalah tersebut.

- Kladaf app merupakan sebuah website yang membantu penggunanya untuk menjadi lebih produktif. Terdapat 7 fitur yang tersedia pada website kladaf antara lain: Log in, Sign in, Podomoro Timer, Weekly Schedule, Progress Tracker, Wishlist, dan Diary. Fungsi dari masing-masing fitur akan lebih dalam dijelaskan pada bagian app module

## Integrasi Dengan Webservices
Integrasi modul dan implementasinya ke dalam Flutter dilakukan dengan melakukan pemanggilan ke beberapa function pada views.py di Django backend berdasarkan beberapa event trigger pada mobile app (contoh: button). Data yang di kirim dari dari webservice ke mobile app diubah dalam bentuk JSON. 

## App Module
### 1. Login
Login merupakan salah satu fitur dalam website Kladaf yang menampilkan formulir yang berisi username serta password yang harus diisi oleh user. Fitur ini akan muncul setiap kali user mencoba untuk mengakses web. Fungsi dari fitur ini yaitu untuk mengidentifikasi apakah user telah terdaftar atau belum terdaftar saat proses Sign In. Apabila user teridentifikasi sebagai member maka mereka dapat masuk dan mengakses halaman web. Sebaliknya, user yang tidak berhasil melakukan login dapat mendaftarkan diri pada Sign In.

### 2. Sign In
Sign in merupakan salah satu fitur yang berfungsi untuk tempat mendaftar dari user atau pengguna website Kladaf. Fitur ini harus dilakukan untuk menggunakan fitur-fitur lainnya dalam website Kladaf, dan hanya perlu dilakukan sekali saja untuk pertama kali membuat akun atau mendaftar menjadi member. Fitur ini akan menyediakan formulir yang berisi beberapa data dari user untuk menyimpan data diri dari user. Dengan adanya fitur ini, semua hal yang sudah dilakukan atau data yang sudah pernah dimasukkan ke dalam website akan tersimpan dalam akun tersebut. Jadi ketika seseorang sudah melakukan sign in, ketika ia ingin mengakses akunnya tinggal melakukan login dengan memasukkan username dan password akunnya.

### 3. PodomoroTimer
Podomoro Timer merupakan salah satu fitur pendukung produktivitas yang tersedia pada website Kladaf. Fungsi dari fitur Podomoro Timer adalah membantu penggunanya menerapkan teknik belajar atau bekerja secara Podomoro. Teknik Podomoro adalah salah satu solusi manajemen waktu yang dikembangkan oleh Francesco Cirillo pada tahun 1980-an. Teknik Podomoro dilakukan dengan mengatur waktu untuk fokus bekerja selama 25 menit, lalu beristirahat selama 5 menit. Pada sesi keempat Podomoro dimana seseorang tersebut telah bekerja 25 menit sebanyak 4 kali, waktu istirahat di Podomoro keempat menjadi lebih panjang yakni 15 - 20 menit. Dengan fitur Podomoro Timer, pengguna bisa menggunakan timer tersebut untuk menerapkan teknik podomoro dan mendapatkan notifikasi apabila waktu bekerja 25 menit dan waktu istirahat telah selesai. Fitur ini diharapkan mampu membantu pengguna untuk lebih fokus dalam menjalani kegiatan sehari-hari, seperti bekerja maupun belajar.

### 4. Weekly Schedule
Weekly Schedule merupakan salah satu fitur pendukung yang tersedia pada website Kladaf. Fungsi dari Weekly Schedule adalah mengatur jadwal yang berulang (repeated schedule). Aplikasi ini sangat berguna untuk menjadi pengingat jadwal sehingga apabila seorang individu lupa karena satu dan lain hal, aplikasi tersebut dapat menjadi reminder bagi sang individu.

### 5. Progress Tracker
Progress Tracker merupakan salah satu fitur pendukung yang tersedia pada website Kladaf. Fungsi dari Progress Tracker adalah membantu pengguna untuk mengetahui pekerjaan apa saja yang belum dimulai, pekerjaan yang sedang dalam proses pengerjaan, pekerjaan yang sedang tahap tahap review, serta pekerjaan yang telah selesai dikerjakan.

### 6. Wishlist
Wishlist merupakan salah satu fitur pendukung yang tersedia pada website Kladaf. Fungsi dari fitur wishlist ini adalah membuat daftar keinginan pengguna baik dalam bentuk benda, destinasi tempat, dan impian-impian lainnya. Pengguna bisa menambahkan daftar wishlist yang ada sesuai dengan apa yang menjadi keinginannya. Jika salah satu keinginan telah berhasil dicapai, pengguna dapat menghapus dari daftar wishlist nya tersebut. Dengan begitu, pengguna merasa terus termotivasi untuk mencapai semua keinginannya. Salah satu caranya adalah dengan tetap produktif.

### 7. Diary
Diary merupakan salah satu fitur pendukung yang tersedia pada website Kladaf. Fitur ini memungkinkan user untuk menulis sebuah diary yang dapat diatur agar bersifat private ataupun public (dapat dilihat oleh orang lain). Tujuan dari fitur ini adalah menyediakan wadah bagi user yang ingin menuliskan keluh-kesahnya dalam beraktivitas di masa pandemi. Selain itu, user juga dapat menggunakan diary sebagai memo untuk menulis pengalaman dan keseharian user.

### 8. Rating & review
Rating merupakan salah satu fitur yang mana dia akan menjadi tempat bagi user atau pengguna ketika ingin memberi saran dan juga memberi nilai terhadap kinerja web kami.

### 9. User Profile
User Profile adalah fitur pendukung yang menampilkan informasi dari user yang telah terotentikasi saat proses Sign In. Informasi user yang ditampilkan mencakup foto profil, nama dari user, serta hobi. Pada fitur ini, user juga dapat mengedit informasi tersebut sesuai keinginannya. 


